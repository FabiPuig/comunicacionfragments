package fabianpuig.example.com.comunicacionfragments;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements Uno.SendMessage{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setInitialFragment();
    }

    private void setInitialFragment(){
        Class fragmentClass = Uno.class;
        Fragment fragment = null;
        try {
            fragment = (Fragment)fragmentClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        getSupportFragmentManager().beginTransaction().add( R.id.fl_content, fragment, "One").commit();
    }

    @Override
    public void sendMessage(String message) {

        Log.i("test", "Mensaje: " + message );
    }
}
