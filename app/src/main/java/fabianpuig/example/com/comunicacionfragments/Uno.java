package fabianpuig.example.com.comunicacionfragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class Uno extends Fragment {

    private EditText editText;
    private Button btSend;

    private SendMessage interfaz;

    public Uno() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_uno, container, false);

        editText = view.findViewById( R.id.editText );
        btSend = view.findViewById( R.id.bt_send );

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                interfaz.sendMessage( editText.getText().toString() );
            }
        });


        return view;
    }

    /** Interfaz para comunicarnos con el Activity
     *
     */
    public interface SendMessage{
        public void sendMessage( String message );
    }

    /** El fragment captura la implementación de la interfaz durante su ciclo de vida onAttach()
     *  y poder llamar los metodos de interfaz para comunicarse con el Activity
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            interfaz = (SendMessage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

}
